#!/usr/bin/env bash
set -euo pipefail

cd "$(dirname "$0")"
terraform apply \
  -target google_service_account.chef_bootstrap \
  -target google_storage_bucket.chef_bootstrap \
  -target google_storage_bucket_iam_binding.chef_bootstrap \
  -target google_kms_crypto_key_iam_binding.chef_bootstrap
