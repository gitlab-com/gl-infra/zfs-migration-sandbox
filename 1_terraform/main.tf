# Set below with env vars: TF_VAR_$var_name

variable "name" {}
variable "project" {}
variable "region" {}
variable "chef_role" {}

variable "gitaly_node_count" {
  default = 1
}

variable "gitaly_disks_per_node" {
  default = 3
}

# See tf-init.sh and .envrc for values passed in as -backend-config
terraform {
  backend "gcs" {}
}

# Configured by GOOGLE_PROJECT and GOOGLE_REGION
provider "google" {
  version = "~> 2.6.0"
}

# Targetted by first-run.sh

resource "google_service_account" "chef_bootstrap" {
  account_id = "chef-bootstrap"
}

resource "google_storage_bucket" "chef_bootstrap" {
  name          = "${format("%s-chef-bootstrap", var.name)}"
  storage_class = "REGIONAL"
  location      = "${var.region}"
}

resource "google_storage_bucket_iam_binding" "chef_bootstrap" {
  bucket  = "${google_storage_bucket.chef_bootstrap.name}"
  role    = "roles/storage.objectViewer"
  members = ["serviceAccount:${google_service_account.chef_bootstrap.email}"]
}

resource "google_kms_key_ring_iam_binding" "chef_bootstrap" {
  key_ring_id = "global/chef-bootstrap"
  role        = "roles/cloudkms.cryptoKeyDecrypter"
  members     = ["serviceAccount:${google_service_account.chef_bootstrap.email}"]
}

# The test environment itself:

# TODO omnibus

# TODO currently this is just a node with multiple disks. To run gitaly roles,
# or even a base role, we will need environment secrets.
module "gitaly" {
  bootstrap_data_disk = "false"
  bootstrap_version   = 9
  data_disk_count     = "${var.gitaly_disks_per_node}"
  node_count          = "${var.gitaly_node_count}"

  chef_provision        = "${local.chef_provision}"
  chef_run_list         = "\"role[${var.chef_role}]\""
  deletion_protection   = false
  data_disk_size        = 1
  data_disk_type        = "pd-standard"
  log_disk_size         = 1
  dns_zone_name         = "gitlab.com"
  egress_ports          = [80, 443]
  environment           = "${var.name}"
  ip_cidr_range         = "10.10.200.0/24"
  machine_type          = "n1-standard-1"
  name                  = "craigf-test"
  os_disk_type          = "pd-ssd"
  project               = "${var.project}"
  public_ports          = [22]
  service_account_email = "${google_service_account.chef_bootstrap.email}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=craigf/configurable-disk-count"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = "default"
  region                = "${var.region}"
}

locals {
  chef_provision = {
    bootstrap_bucket  = "${google_storage_bucket.chef_bootstrap.name}"
    bootstrap_key     = "chef-bootstrap-key"
    bootstrap_keyring = "chef-bootstrap"

    server_url    = "https://chef.gitlab.com/organizations/gitlab/"
    user_name     = "gitlab-ci"
    user_key_path = ".chef.pem"
    version       = "12.22.5"
  }
}
