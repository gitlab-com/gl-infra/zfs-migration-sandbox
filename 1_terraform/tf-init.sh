#!/usr/bin/env bash
set -euo pipefail

cd "$(dirname "$0")"

terraform init \
  -backend-config "bucket=$BACKEND_BUCKET" \
  -backend-config "prefix=$SANDBOX_NAME" \
