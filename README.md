# zfs-migration-sandbox

## What is this?

Early WIP to provision testbed GitLab environments to test Gitaly node
migrations. GitLab will be provisioned as a single VM except for the Gitaly
fleet for this reason. At the moment not much at all is provisioned.

Most users will want the [environments
project](https://gitlab.com/gitlab-com/environments), particularly the
[single_omnibus
branch](https://ops.gitlab.net/gitlab-com/environments/tree/single_omnibus)
until that is merged. This repository will likely disappear in favour of
environments.

## Usage

### Foundational Pets

These are created manually.

1. A GCP project. "gl-zfs-storage-sandbox" should already exist, but the code
   in this repository should be reusable for new sandbox projects.
1. A terraform state bucket. In the gl-zfs-storage-sandbox project, I created
   gl-zfs-storage-sandbox-tfstate as a regional bucket in europe-west4, with
   bucket-level access control and a Google-managed encryption key.
1. If you are logged into the `gcloud` CLI, you should already have permission
   to terraform the project and read/write the backend state bucket by virtue of
   membership of the `gcp-ops-sg@gitlab.com`Google Group, which should inherit
   sweeping permissions over most GCP resources in the project by IAM
   inheritance from the Master Billing Account.
1. Create a KMS keyring "chef-bootstrap" (this must be in the global region due
   to hardcoding in the bootstrap script, this can always be changed if we
   want), and then a symmetric cryptographic key inside it named
   "chef-bootstrap-key", with a rotation policy of "never".
1. Make sure you are using the version of terraform specified in
   `.terraform-version` in the `1_terraform` directory. Consider using `tfenv`
   to automate this.
1. `cp .envrc.template .envrc` and fill in the appropriate values.
1. Test your privilege with `./tf-init.sh`.

#### Why are the KMS resources manually created?

KMS cryptographic keys and keyrings cannot be truly deleted from GCP projects.
When terraform destroys the resource, it will destroy the key material,
rendering it unusable, but the resource names are left as tombstones to prevent
keys with the same name being recreated. This is a security measure. Since
projects created from this repository are ephemeral, we want to `terraform
destroy` and re-create with some regularity. If we tried to use the same name to
create a keyring or key resource, the operation would fail. If we used
random/unique names, we would clutter the KMS section of the project.

#### Other potential improvements

If we used a GCS bucket that lived outside the sandbox project, we could create
the google project itself using terraform. At the moment we are relatively
unlikely to want to instantiate multiple ZFS sandboxes at once, and if we did we
would be better served by contributing to the
[environments](https://gitlab.com/gitlab-com/environments) project.

### Create initial resources

First, we will terraform just enough infrastructure to upload an encrypted chef
validation private key so that machines will be able to bootstrap themselves.

1. Download and decrypt the chef server validation key from an existing project,
   e.g. gstg. See [the bootstrap
   script](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/bootstrap/blob/master/scripts/bootstrap-v9.sh#L102-104)
   for an example of how to do this.
1. `./tf-init.sh && ./first-run.sh`
1. `gcloud --project $GOOGLE_PROJECT kms encrypt --ciphertext-file
   ./validation.enc --plaintext-file ./validation.pem --keyring chef-bootstrap
   --key chef-bootstrap-key --location global`
1. `gsutil cp ./validation.enc gs://${SANDBOX_NAME}-chef-bootstrap/`
1. **Delete the key material from your local drive**: `rm validation.enc
   validation.pem`.

### Create the environment

`terraform plan` and `terraform apply` to your heart's content. Use targetted
`terraform destroy` to avoid destroying the chef bootstrap bucket and having to
manually recreate that.

### What's next?

1. Provision a single-VM GitLab Omnibus
1. Bootstrap actual Gitaly nodes
